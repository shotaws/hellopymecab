# -*- coding:utf-8 -*-                                                                                                                                            
'''
http://d.hatena.ne.jp/addition/20130405/1365089276
'''
import urllib2
import lxml.html
import MeCab


opener = urllib2.build_opener()
opener.addheaders = [('User-agent', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; YTB720; GTB6.3; SLCC1; .NET CLR 2.0.50727; Media Center PC 5.0; .NET CLR 3.5.30729; .NET CLR 3.0.30618)')]

mecab = MeCab.Tagger("mecabrc")

html = opener.open('http://www.yahoo.co.jp').read()
root = lxml.html.fromstring(html)
texts = root.xpath('//body')
for text in texts:
    print mecab.parse(text.text_content().encode('utf-8')) 
